import React, { Component } from 'react';
import axios from 'axios';

class Forms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      forms: props.forms,
      logged_in: false,
      flag: false
    }
    this.loginFunc = this.loginFunc.bind(this);
  }

  // ensure latest forms data
  componentWillReceiveProps(nextProps) {
    // console.log(nextProps);
    this.setState({ forms: nextProps.forms })
  }

  // this function creates the table rows 
  createFormsTable(forms) {
    // console.log(forms);
    return forms.map(function (form, i) {
      return (
        <tr key={i}>
          <td>{form.first_name}</td>
          <td>{form.last_name}</td>
          {/* press email text to send email */}
          <td><a href={`mailto:${form.email}`}>{form.email}</a></td>
          <td><a target="_blank" href={form.website}>{form.website}</a></td>
          <td><a target="_blank" href={form.linkedin_profile}>{form.linkedin_profile}</a></td>
          <td>{form.join_date}</td>
        </tr>
      )
    })
  }

  // check for user credentials function
  // the user will be logged out whenever he leaves the forms component
  loginFunc(e) {
    let that = this;
    e.preventDefault(); // prevent page refresh
    let username = document.getElementById('form-username');
    let password = document.getElementById('form-password');
    // get login form values
    let details = {
      username: username.value,
      password: password.value
    }
    // login post request
    axios.post("/forms/login", details)
      .then(function (response) {
        console.log(response.data.authenticated);
        if (response.data.authenticated) {
          that.setState({ logged_in: true });
        } else {
          username.value = "";
          password.value = "";
          alert('wrong username and/or password');
          username.focus();
          return false;
        }
      })
      .catch(function (error) {
        console.log(error);
      })
  }

  // sort by prop name func
  dynamicSort(prop, flag) {
    return function (a, b) {
      if (flag) {
        return (a[prop] < b[prop]) ? -1 : (a[prop] > b[prop]) ? 1 : 0;
      } else {
        return (a[prop] > b[prop]) ? -1 : (a[prop] < b[prop]) ? 1 : 0;
      }
    };
  }
  //main function to sort forms arr by property name
  sortFormsByProp(prop) {
    let tempForms = this.state.forms;
    let flag = this.state.flag;
    tempForms.sort(this.dynamicSort(prop, flag));
    this.setState({
      forms: tempForms,
      flag: !flag // reverse the order of the sort next time
    })
  }

  render() {
    // if user is logged in then show forms
    if (this.state.logged_in) {
      return (
        <div id="forms-table" className="container text-center">
          <h2>Forms List</h2>
          <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic fugit, sint quaerat repellat iusto sunt accusantium maxime eveniet, sequi expedita laboriosam, quisquam aut similique quidem!</p>
          <hr />
          <h3 style={{ color: "white" }}>The table is sortable by name, email and join date (click on the table headers)</h3>
          <table className="text-center table table-striped table-responsive">
            <thead>
              <tr>
                {/* when clicking on table headers call sort function with propery name */}
                <th onClick={this.sortFormsByProp.bind(this, 'first_name')}>Firstname</th>
                <th onClick={this.sortFormsByProp.bind(this, 'last_name')}>Lastname</th>
                <th onClick={this.sortFormsByProp.bind(this, 'email')}>Email</th>
                <th>Website</th>
                <th>LinkedIn</th>
                <th onClick={this.sortFormsByProp.bind(this, 'join_date')}>Joined</th>
              </tr>
            </thead>
            <tbody>
              {/* returning a map of the forms to display */}
              {this.createFormsTable(this.state.forms)}
            </tbody>
          </table>
        </div>
      );
    } else { // else show login form
      return (
        <div className="container">
          <form onSubmit={this.loginFunc} role="form" action="" className="login-form">
            <div className="form-group">
              {/* <label className="sr-only">First Name</label> */}
              <input type="text" name="form-fname" placeholder="Username" className="form-username form-control" id="form-username" />
            </div>
            <div className="form-group">
              <input type="password" name="form-lname" placeholder="Password" className="form-username form-control" id="form-password" />
            </div>
            <hr />
            <button id="form-submit" type="submit" className="btn">Login</button>
          </form>
        </div>
      )
    }

  }
}

export default Forms;