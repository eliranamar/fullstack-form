import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// this component is the app navbar
class Header extends Component {
  render() {
    return (
      <nav className="navbar navbar-inverse">
        <div className="container-fluid">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <Link className="navbar-brand" to="/">React-Website</Link>
          </div>
          <div className="collapse navbar-collapse" id="myNavbar">
            <ul className="nav navbar-nav">
              <li role="presentation"><Link to="/">Home</Link></li>
              <li role="presentation"><Link to="/forms">Forms</Link></li>
              <li role="presentation"><Link to="/about">About Us</Link></li>
            </ul>
          </div>
        </div>
      </nav>

    );
  }
}

export default Header;