import React, { Component } from 'react';

class About extends Component {
  render() {
    return (
      <div>
        <div className="container text-center">
          <h1>About Us</h1>
          <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum sit suscipit maxime quam adipisci. Eaque explicabo asperiores et? Quo repellat doloremque vitae perspiciatis eius aperiam dignissimos officia voluptatibus incidunt laborum possimus optio iusto autem consequuntur earum sed, mollitia iure blanditiis deserunt repudiandae dolorum, amet voluptas sequi quaerat. Voluptas molestiae ullam, rerum ducimus impedit, eveniet quos a autem blanditiis, sapiente reprehenderit ea cum illum animi corrupti unde labore culpa laborum. Aspernatur consequuntur temporibus beatae ad doloremque, provident obcaecati dicta fuga odit dignissimos, quam soluta neque corporis aperiam vitae sunt odio ducimus reiciendis minima debitis accusantium accusamus iste! Quas impedit temporibus numquam!</span>
        </div>
      </div>
    );
  }
}

export default About;