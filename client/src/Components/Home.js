import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formSent: props.formSent,
      numOfForms: props.numOfForms,
      setFormSentState: props.setFormSentState
    }
    this.formSubmit = this.formSubmit.bind(this);
  }

  // when updating the main app state (formSent), update it here also
  componentWillReceiveProps(nextProps) {
    this.setState({
      formSent: nextProps.formSent,
      numOfForms: nextProps.numOfForms
    })
  }

  // for capitalizing first letter
  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  // submit form func
  formSubmit(e) {
    e.preventDefault(); // prevent page refresh
    // debugger
    let that = this;
    // probably there is a better way to collect form data :-/
    let submitButton = document.getElementById('form-submit');
    // prevent double click
    submitButton.disabled = true;
    let fname = document.getElementById('form-fname').value;
    // trim the white spaces before and after the name
    fname = fname.trim();
    //then capitalize first letter
    fname = this.capitalizeFirstLetter(fname);
    let lname = document.getElementById('form-lname').value;
    lname = lname.trim();
    lname = this.capitalizeFirstLetter(lname);
    let email = document.getElementById('form-email').value;
    // to lower case email
    email = email.toLowerCase();
    let website = document.getElementById('form-website').value;
    let linkedin = document.getElementById('form-linkedin').value;
    let exp = document.getElementById('form-exp').value;
    let budget = document.getElementById('form-budget').value;
    let budgetChecked = document.getElementById('form-no-budget').checked;
    console.log(budgetChecked);
    if (budgetChecked) {
      budget = "rather not say";
    }
    // also add joined date and time:
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    let yyyy = today.getFullYear();
    let hours = today.getHours();
    let minutes = today.getMinutes();
    // make it look nice:
    if (dd < 10) { dd = '0' + dd };
    if (mm < 10) { mm = '0' + mm };
    // join to 1 date string
    today = mm + '/' + dd + '/' + yyyy + " " + hours + ":" + minutes;
    let form = {
      first_name: fname,
      last_name: lname,
      email: email,
      website: website,
      linkedin_profile: linkedin,
      years_experience: exp,
      biggest_campaign: budget,
      join_date: today
    }
    console.log(form);

    // post request to save the form data in DB
    axios
      .post("/forms/signup", form)
      .then(function (response) {
        console.log(response.data);
        // if server not validated email
        if (response.data.saved == 'invalid') {
          alert('INVALID EMAIL SENT TO SERVER');
          return false;
        }
        // if email already signed up
        if (response.data.saved) {
          that.props.setFormSentState(true);
          // console.log(this);
          // console.log(that);
        } else {
          alert('you are already signed up with this email');
          submitButton.disabled = false;
          // console.log(this);
          // console.log(that);
        }
      })
      .catch(function (error) {
        console.log(error);
        if (error.request.status === 401) {
          alert("UNAUTHORIZED. PLEASE LOGIN");
          // history.pushState(null, null, '/');
          // return <Redirect to="/" />;
        }
      });
  }

  // focus on the first name input when component loaded
  componentDidMount() {
    document.getElementById('form-fname').focus();
    let slider = document.getElementById("form-budget");
    let output = document.getElementById("slider-value");
    output.innerHTML = slider.value;

    slider.oninput = function () {
      output.innerHTML = this.value;
    }
  }

  render() {


    // if the form sent say thanks
    if (this.state.formSent) {
      return (
        <div>
          <div className="text-center">
            <h1>Thanks you for signing to our service !</h1>
            <h3>We will be in touch soon...</h3>
          </div>
        </div>
      );
    } else { // else show the form
      return (
        <div className="container">
          <div className="row">
            <div className="col-sm-8 col-sm-offset-2 text-center">
              <h1><strong>MyCompany</strong> SignUp Form</h1>
              <hr />
              <div className="description">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam cupiditate placeat consequuntur sit perferendis deleniti a repellat recusandae velit ea!</p>
              </div>
              <h2>{this.state.numOfForms} Marketers Joined So Far</h2>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-8 col-sm-offset-2 form-box">
              <div className="form-top">
                <div className="form-top-left">
                  <h3>Sign up to our service</h3>
                  <p>Enter your details:</p>
                </div>
                <div className="form-top-right">
                  <i className="fa fa-thumbs-up"></i>
                </div>
              </div>
              <div className="form-bottom">
                <form onSubmit={this.formSubmit} role="form" action="" method="post" className="login-form">
                  <div className="form-group">
                    {/* <label className="sr-only">First Name</label> */}
                    <input type="text" name="form-fname" placeholder="First Name (Optional)" className="form-username form-control" id="form-fname" />
                  </div>
                  <div className="form-group">
                    <input type="text" name="form-lname" placeholder="Last Name (Optional)" className="form-username form-control" id="form-lname" />
                  </div>
                  {/* used RegEx pattern to validate email */}
                  <div className="form-group">
                    <input required type="email" name="form-email" placeholder="Email (Requiered)" className="form-username form-control" id="form-email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" />
                  </div>
                  <div className="form-group">
                    <input type="text" name="form-website" placeholder="Your Website (Optional)" className="form-username form-control" id="form-website" />
                  </div>
                  <div className="form-group">
                    <input type="text" name="form-linkedin" placeholder="Your LinkedIn Profile URL (Optional)" className="form-username form-control" id="form-linkedin" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="form-exp">Years Of Experience (Optional):</label>
                    <select className="form-control" id="form-exp">
                      <option>rather not say</option>
                      <option>no experience</option>
                      <option>0-1 years</option>
                      <option>1-2 years</option>
                      <option>2 or more years</option>
                    </select>
                  </div>
                  {/* <div className="form-group">
                    <label htmlFor="form-budget">Biggest Campaign Budget Managed in USD$:</label>
                    <input type="text" name="form-budget" placeholder="Enter range 1000$ - 500,000$ (Optional)" className="form-username form-control" id="form-bffudget" />
                  </div> */}
                  <div id="slidecontainer">
                    <label htmlFor="form-budget">Biggest Campaign Budget Managed in USD$ (Optional):</label>
                    <p>Enter range 1000$ - 500,000$</p>
                    <div className="checkbox">
                      <label><input type="checkbox" value="" id="form-no-budget" />Rather not say</label>
                    </div>
                    <input type="range" min="1000" max="500000" step="500" className="slider" id="form-budget" />
                    <p>You Entered: <span id="slider-value"></span></p>
                  </div>
                  <hr />
                  <button id="form-submit" type="submit" className="btn">Sign Up!</button>
                  <hr />
                  <button id="form-reset" type="reset" className="btn btn-danger">Reset Form</button>
                </form>
              </div>
            </div>
          </div>
          <div id="features-box" className="features-container section-container">
            <div>
              <div className="row">
                <div className="col-sm-12 features section-description wow fadeIn">
                  <h2>Why You Should Join?</h2>
                  <div className="divider-1 wow fadeInUp">. . . . . . . . . .</div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.
	                    	Ut wisi enim ad minim veniam, quis nostrud tempor incididunt ut labore et.</p>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-4 features-box wow fadeInUp">
                  <div className="features-box-icon"><i className="fa fa-thumbs-up"></i></div>
                  <h3>Easy To Use</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                </div>
                <div className="col-sm-4 features-box wow fadeInDown">
                  <div className="features-box-icon"><i className="fa fa-cog"></i></div>
                  <h3>Responsive Design</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                </div>
                <div className="col-sm-4 features-box wow fadeInUp">
                  <div className="features-box-icon"><i className="fa fa-commenting"></i></div>
                  <h3>24/7 Support</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                </div>
              </div>

            </div>
          </div>
        </div>
      )
    }
  }
}

export default Home;