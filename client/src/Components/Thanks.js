import React, { Component } from 'react';

class Thanks extends Component {
  render() {
    return (
      <div>
        <div className="text-center">
          <h1>Thanks you for signing to our service !</h1>
          <h3>We will be in touch soon...</h3>
        </div>
      </div>
    );
  }
}

export default Thanks;