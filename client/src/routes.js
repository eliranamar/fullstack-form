import React from 'react';
import Home from './Components/Home'
import Forms from './Components/Forms'
import About from './Components/About'
import Page404 from './Components/Page404'
import { Switch, Route, Redirect } from 'react-router-dom';

// switch components by page url
const Routes = (props) => (
  <div>
    <Switch>
      <Route name="home" exact path='/' render={() => <Home numOfForms={props.numOfForms} formSent={props.formSent} setFormSentState={props.setFormSentState} />} />
      <Route name="forms" exact path='/forms' render={() => <Forms forms={props.forms} />} />
      <Route name="about" exact path='/about' render={() => <About />} />
      {/* in case no route is handled show 404 error */}
      <Route path="*" component={Page404} />
    </Switch>
  </div>
)

export default Routes;