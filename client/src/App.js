import React, { Component } from 'react';
import ReactDom from 'react-dom';
import axios from 'axios'
import Header from './Components/Header';
import Routes from './Routes';
import { BrowserRouter } from 'react-router-dom';

// this is the main app component
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      forms: [],
      numOfForms: 0,
      formSent: false
    };
    // binding the "this" for this function that will sent to Home component
    this.setFormSentState = this.setFormSentState.bind(this);
  }


  // when app is loaded
  componentWillMount() {
    let that = this;
    (function () {
      // let user = JSON.parse(localStorage.getItem("user"));
      axios
        .get("/forms/getallforms")
        .then(function (response) {
          // console.log(response.data.length);
          that.setState({ forms: response.data, numOfForms: response.data.length });
        })
        .catch(function (error) {
          console.log(error);
        });
    })();
  }


  // change the state of formSent variable
  setFormSentState (sent) {
    this.setState({formSent:sent});
  }

  render() {
    // console.log('Main App Rendered');
    return (
      <BrowserRouter>
        <div>
          <Header />
          <Routes numOfForms={this.state.numOfForms} forms={this.state.forms} formSent={this.state.formSent} setFormSentState={this.setFormSentState}/>
        </div>
      </BrowserRouter>

    );
  }
}

ReactDom.render(
  <App />, document.getElementById('react-app'));