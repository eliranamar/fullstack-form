let express = require('express');
let router = express.Router();
let Form = require("../models/FormModel");

// Handles Success / Failure , and returns data
let handler = function (res, next) {
  return function (err, data) {
    if (err) {
      return next(err);
    }
    res.send(data);
  }
}

// for getting all the forms data
router.get('/getallforms', function (req, res, next) {
  Form.find(function (err, forms) {
    if (err) {
      console.log(err);
      return next(err);
    } else {
      console.log('---  FETHING FORMS FROM DB ---');
      // console.log(forms);
      res.send(forms);
    }
  })
});

// server side email validation func
function validateEmail(email) {
  // First check if any value was actually set
  if (email.length == 0) return false;
  // Now validate the email format using Regex
  let regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
  return regex.test(email);
}

// to fetch all the forms data
router.post('/signup', function (req, res, next) {
  let email = req.body.email;
  // validate email in server
  if (validateEmail(email) == false) {
    res.send({ saved: 'invalid' });
  } else {
    let FormToSave = new Form(req.body);
    // check if email already in database
    Form.findOne({ email: FormToSave.email }, function (err, foundEmail) {
      if (err) {
        console.log(err);
        return next(err);
      }
      // if not found than save
      if (!foundEmail) {
        FormToSave.save();
        console.log('FORM SAVED');
        res.send({ saved: true });
      } else { // else dont..
        // console.log('email already in database');
        res.send({ saved: false });
      }
    })
  }
});

// hard coded login auth
router.post('/login', function (req, res, next) {
  let username = req.body.username;
  let password = req.body.password;
  if (username === "admin" && password === "password") {
    res.send({ authenticated: true });
  } else {
    res.send({ authenticated: false });
  }
})

module.exports = router;