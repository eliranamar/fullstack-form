let mongoose = require('mongoose');

// form DB schema
const formSchema = new mongoose.Schema({
  first_name: String,
  last_name: String,
  email: {type:String, required:true},
  website: String,
  linkedin_profile: String,
  years_experience: String,
  biggest_campaign: String,
  join_date: String
});

let Form = mongoose.model('form', formSchema);

module.exports = Form;