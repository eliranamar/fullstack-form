const express = require('express');
const bodyParser = require('body-parser')
let mongoose = require('mongoose');
let path = require("path");
let app = express();
// bring the formroutes file
let formRoutes = require('./routes/formRoutes');

// connect to DB 
mongoose.connect(process.env.CONNECTION_STRING || 'mongodb://localhost/forms-test', function () {
  console.log("DB connection established!!!");
})

// serve static files
app.use(express.static("./static/"));
app.use(express.static("./client/dist/"));
app.use(express.static('node_modules'));
// use body parser for req.body access
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//This tells the server that when a request comes into '/forms'
//that it should use the routes in 'formRoutes'
//and those are in our new formRoutes.js file
app.use('/forms', formRoutes);


//Handle browser refresh by redirecting to index html
app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname, "./static/index.html"));
});

// for unhabdled routes return to home
app.all('[^.]+', function (req, res) {
  res.sendFile(__dirname + "/static/index.html");
});

// error handler to catch 404 and forward to main error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// main error handler
// warning - not for use in production code!
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send({
    message: err.message,
    error: err
  });
});

app.listen(process.env.PORT || '3000', function () {
  console.log("app live on localhost:3000  :-)")
});